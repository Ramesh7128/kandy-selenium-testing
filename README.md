### About

A golang script to load test kandy-labs with selenium and chromedriver.

### Installation for Mac OS

Install the chromedriver for selenium to work.

```
brew install chromedriver
```

### Initial Setup

Copy the contents of .env.example file and create a .env file in the same root folder.

Add basic auth username and password to the .env file (password and username needs to be url encoded. https://www.urlencoder.org/)

Add the set of paths that needs to be tested to url.txt file with a leading forward slash.(one each line)

```
eg :
/pricing/self-service-apis/chat
/turnkey-apps/use-cases/contact-center-with-chatbot
```
### Running Locally

execute the kandy-loadtesting binary present in the root directory passing the required arguments.

```
./kandy-loadtesting <-randroute> <labname> <no of browser window> <filename>
  -randroute flag to add random string to the end of urls to generate unique urls in each tab.(optional)
(or)
./kandy-loadtesting <labname> <no of browser window> <filename>


./kandy-loadtesting nvs 10 urls.txt

./kandy-loadtesting alderaan 10 urls.txt

./kandy-loadtesting -randroute alderaan 10 urls.txt

./kandy-loadtesting kvs 10 urls.txt
```




