package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"math/rand"
	"os"
	"os/exec"
	"strconv"
	"sync"
	"time"

	"github.com/joho/godotenv"
	"github.com/tebeka/selenium"
)

const (
	seleniumPath = "chromedriver"
	port         = 4444
)

func init() {
	// close all existing chrome tabs.
	cmd := exec.Command("/bin/sh", "./setup.sh")
	err := cmd.Run()
	if err != nil {
		fmt.Println(err)
	} else {
		time.Sleep(4 * time.Second)
	}

	// loads values from .env into the system
	if err := godotenv.Load(); err != nil {
		log.Print("No .env file found")
	}
}

func randomString(n int) string {
	charSet := "abcdedfghijklmnopqrstuvwxyz"
	b := make([]byte, n)
	for i := range b {
		b[i] = charSet[rand.Intn(len(charSet))]
	}
	return string(b)
}

func readFilePaths(fileName string) []string {
	readFile, err := os.Open(fileName)
	if err != nil {
		log.Fatalf("Failed to open file :%s", err)
	}
	fileScanner := bufio.NewScanner(readFile)
	fileScanner.Split(bufio.ScanLines)

	var fileLines []string
	for fileScanner.Scan() {
		text := fileScanner.Text()
		if len(text) != 0 {
			fileLines = append(fileLines, text)
		}
	}
	readFile.Close()
	return fileLines
}

type browserOptions struct {
	authBaseURL string
	baseURL     string
	fileLines   []string
	wg          *sync.WaitGroup
	wm          *sync.Mutex
	randPtr     bool
}

func createSeleniumRemoteClientList(count int) []selenium.WebDriver {
	caps := selenium.Capabilities{
		"browserName": "chrome",
	}
	var remoteClientList []selenium.WebDriver
	for i := 0; i < count; i++ {
		wd, err := selenium.NewRemote(caps, "http://127.0.0.1:4444/wd/hub")
		if err != nil {
			fmt.Println("Error creating the remote client", err)
		} else {
			remoteClientList = append(remoteClientList, wd)
		}
	}
	return remoteClientList
}

func asynBrowserCalls(options browserOptions, wd selenium.WebDriver) {
	wd.Get(options.authBaseURL)
	time.Sleep(30 * time.Second)
	// To test if the basic auth worked
	wd.Get(options.baseURL)

	urlCount := len(options.fileLines)

	switch true {
	case urlCount > 1:
		for _, eachURL := range options.fileLines {
			var url string
			if options.randPtr {
				url = options.baseURL + eachURL + randomString(3)
			} else {
				url = options.baseURL + eachURL
			}
			wd.ExecuteScript("window.open('"+url+"');", nil)
		}
	case urlCount == 1:
		var url string
		if options.randPtr {
			url = options.baseURL + options.fileLines[0] + randomString(3)
		} else {
			url = options.baseURL + options.fileLines[0]
		}
		wd.Get(url)
	default:
		log.Fatal("No url paths provided")
		break
	}

	options.wg.Done()
}

func createBaseURLS(platform string) (authBaseURL, baseURL string) {
	user, exists := os.LookupEnv("PORTAL_USER")
	if !exists {
		log.Print("No user value set in .env file")
	}

	password, exists := os.LookupEnv("PASSWORD")
	if !exists {
		log.Print("No password value set in .env file")
	}

	switch platform {
	case "alderaan":
		authBaseURL = "https://" + user + ":" + password + "@alderaan-apimarket.kandycorp.net"
		baseURL = "https://alderaan-apimarket.kandycorp.net"
	case "nvs":
		authBaseURL = "https://" + user + ":" + password + "@nvs-apimarket.kandy.io"
		baseURL = "https://nvs-apimarket.kandy.io"
	case "kvs":
		authBaseURL = "https://" + user + ":" + password + "@kvs1-apimarket.kandy.io"
		baseURL = "https://kvs1-apimarket.kandy.io"
	default:
		authBaseURL = "https://" + user + ":" + password + "@alderaan-apimarket.genband.com"
		baseURL = "https://alderaan-apimarket.genband.com"
	}

	return authBaseURL, baseURL
}

func main() {
	rand.Seed(time.Now().UnixNano())
	randPtr := flag.Bool("randroute", false, "a bool")
	flag.Parse()
	fmt.Println(flag.Args(), "this will be flag args")
	platform := flag.Args()[0]
	tabs, _ := strconv.Atoi(flag.Args()[1])
	fileName := flag.Args()[2]
	fileLines := readFilePaths(fileName)

	fmt.Println(platform, tabs, fileName, "these are the argument")

	authBaseURL, baseURL := createBaseURLS(platform)
	ops := []selenium.ServiceOption{}
	_, err := selenium.NewChromeDriverService(seleniumPath, port, ops...)
	if err != nil {
		fmt.Printf("Error starting the ChromeDriver server: %v", err)
	}
	//Delay service shutdown
	// defer service.Stop()

	remoteClientList := createSeleniumRemoteClientList(tabs)

	var wg sync.WaitGroup
	var wm sync.Mutex
	start := time.Now()

	options := browserOptions{
		authBaseURL: authBaseURL,
		baseURL:     baseURL,
		fileLines:   fileLines,
		wg:          &wg,
		wm:          &wm,
		randPtr:     *randPtr,
	}

	fmt.Println(options.randPtr, "this will be the random ptr")

	for i := 0; i < tabs; i++ {
		wg.Add(1)
		go asynBrowserCalls(options, remoteClientList[i])
	}

	wg.Wait()
	end := time.Now()
	diff := end.Sub(start)
	fmt.Printf("Time Run: %v\n", diff)
}
